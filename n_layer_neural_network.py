import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt
from three_layer_neural_network import NeuralNetwork

def generate_data():
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
    #X, y = datasets.make_moons(200, noise=0.20)
    X, y = datasets.make_circles(300, noise=0.5, factor=0.5, random_state=1);
    
    return X, y

def plot_decision_boundary(pred_func, X, y):
    '''
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    '''
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.title('Number of layers: 3; layer sizes: 2, 30, 2; activation function: relu')
    plt.show()

########################################################################################################################
########################################################################################################################
# YOUR ASSSIGMENT STARTS HERE
# FOLLOW THE INSTRUCTION BELOW TO BUILD AND TRAIN A 3-LAYER NEURAL NETWORK
########################################################################################################################
########################################################################################################################

class DeepNeuralNetwork(NeuralNetwork):
    """
    This class builds and trains a neural network
    """
    def __init__(self, layers, actFun_type='Tanh', reg_lambda=0.01, seed=0):
         
        '''
        :param layers: array of the number of units in each layer
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        :param params: model params including W, b, dW, db
        '''
        
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda
        self.layers = layers
        self.params = {}
        # initialize the weights and biases in the network
        np.random.seed(seed)
        for i in range(0, len(self.layers)-1):
            self.params['W%d'%(i+1)] = np.random.randn(self.layers[i], self.layers[i+1]) / np.sqrt(self.layers[i])
            self.params['b%d'%(i+1)] = np.zeros((1, self.layers[i+1]))
            
    def actFun(self, z, type):
        '''
        actFun computes the activation functions
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: activations
        '''

        # YOU IMPLMENT YOUR actFun HERE
        activation = None
        if type == 'Tanh':
            activation = np.tanh(z)
        if type == 'Sigmoid':
            activation = 1.0/(1.0 + np.exp(-z))
        if type == 'ReLU':
            activation = np.maximum(z, 0)
        return activation


    def diff_actFun(self, z, type):
        '''
        diff_actFun computes the derivatives of the activation functions wrt the net input
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: the derivatives of the activation functions wrt the net input
        '''

        # YOU IMPLEMENT YOUR diff_actFun HERE
        derivative = None  
        if type == "Tanh":
            derivative = 1 - np.tanh(z)**2
        if type == "Sigmoid":
            derivative = (1.0/(1.0 + np.exp(-z)))*(1-1.0/(1.0 + np.exp(-z)))
        if type == "ReLU":
            z[z<=0] = 0
            z[z>0] = 1
            derivative = z
        return derivative

    def feedforward(self, X, actFun):
        '''
        feedforward builds a n-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return: none
        '''

        # YOU IMPLEMENT YOUR feedforward HERE
        self.params['a0'] = X
        for i in range(0, len(self.layers)-2):
            self.params['z%i'%(i+1)] = np.dot(self.params['a%i'%i], self.params['W%i'%(i+1)])+self.params['b%i'%(i+1)]
            self.params['a%i'%(i+1)] = self.actFun(self.params['z%i'%(i+1)], self.actFun_type)          
        self.params['z%i'%(len(self.layers)-1)] = np.dot(self.params['a%i'%(len(self.layers)-2)], self.params['W%i'%(len(self.layers)-1)])+self.params['b%i'%(len(self.layers)-1)]

        exp_scores = np.exp(self.params['z%i'%(len(self.layers)-1)])
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return None

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
        # Calculating the loss

        # YOU IMPLEMENT YOUR CALCULATION OF THE LOSS HERE
        logprobs = -np.log(self.probs[range(num_examples),y])
        data_loss = np.sum(logprobs)
        
        # Add regulatization term to loss (optional)
        for i in range(0, len(self.layers)-1):
            data_loss += self.reg_lambda / 2 * np.sum(np.square(self.params['W%d'%(i+1)]))
        print (1. / num_examples) * data_loss     
        return (1. / num_examples) * data_loss

    def predict(self, X):
        '''
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        '''
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
        return np.argmax(self.probs, axis=1)

    def backprop(self, X, y):
        '''
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: none
        '''

        # IMPLEMENT YOUR BACKPROP HERE
        num_examples = len(X)
        self.params['delta%d'%(len(self.layers))] = self.probs  
        self.params['delta%d'%(len(self.layers))][range(num_examples), y] -= 1

        for i in range(len(self.layers)-1, 1, -1):
            self.params['dW%d'%i] = np.dot((self.params['a%d'%(i-1)].T), self.params['delta%d'%(i+1)])
            self.params['db%d'%i] = np.sum(self.params['delta%d'%(i+1)], axis = 0)         
            self.params['delta%d'%i] = np.dot(self.params['delta%d'%(i+1)], self.params['W%d'%i].T) * self.diff_actFun(self.params['z%d'%(i-1)], self.actFun_type)
        self.params['dW1'] = np.dot(X.T, self.params['delta2'])
        self.params['db1'] = np.sum(self.params['delta2'], axis = 0)
        
        return None

    def fit_model(self, X, y, epsilon=0.01, num_passes=20000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for n in range(0, num_passes):
          
            # Forward propagation
            self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
            
            # Backpropagation
            self.backprop(X, y)
            
            # Add regularization terms (b1 and b2 don't have regularization terms)
            for i in range(0, len(self.layers)-1):
                self.params['dW%d'%(i+1)] += self.reg_lambda * self.params['W%d'%(i+1)]
                
            # Gradient descent parameter update
            for i in range(0, len(self.layers)-1):
                self.params['W%d'%(i+1)] += -epsilon * self.params['dW%d'%(i+1)]
                self.params['b%d'%(i+1)] += -epsilon * self.params['db%d'%(i+1)]
           
            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.    
            if print_loss == True and n % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        '''
        visualize_decision_boundary plots the decision boundary created by the trained network
        :param X: input data
        :param y: given labels
        :return:
        '''
        plot_decision_boundary(lambda x: self.predict(x), X, y)
        

def main():
    # # generate and visualize Make-Moons dataset
    X, y = generate_data()
    #plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    #plt.show()

    model = DeepNeuralNetwork([2, 5, 3, 2], actFun_type='Sigmoid')   
    model.fit_model(X,y)
    model.visualize_decision_boundary(X,y)


if __name__ == "__main__":
    main()